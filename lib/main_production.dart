// Copyright (c) 2023, Very Good Ventures
// https://verygood.ventures
//
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.

import 'package:supercharge_ff_kristof/app/app.dart';
import 'package:supercharge_ff_kristof/bootstrap.dart';
import 'package:injectable/injectable.dart';


void main() {
  bootstrap(App.new, environment: Environment.prod);
}
