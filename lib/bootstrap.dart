// Copyright (c) 2023, Very Good Ventures
// https://verygood.ventures
//
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.

import 'dart:async';
import 'dart:ui';

import 'package:bloc/bloc.dart';
import 'package:crashlytics_service/crashlytics_service.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:easy_logger/easy_logger.dart' hide BuildMode;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:injectable/injectable.dart';
import 'package:platform_info/platform_info.dart';
import 'package:stack_trace/stack_trace.dart' as stack_trace;
import 'package:supercharge_ff_kristof/app/core/state/app_cubit_state.dart';
import 'package:supercharge_ff_kristof/app/core/state/app_state.dart';
import 'package:supercharge_ff_kristof/features/random/domain/entities/random_beer_entity.dart';
import 'package:talker_bloc_logger/talker_bloc_logger.dart';
import 'package:talker_flutter/talker_flutter.dart';

import 'package:supercharge_ff_kristof/gen/gen.dart';
import 'app/app.dart';

class CrashlyticsTalkerObserver extends TalkerObserver {
  CrashlyticsTalkerObserver(this.crashlyticsService);
  final CrashlyticsService? crashlyticsService;

  @override
  void onError(TalkerError err) {
    /// Send data to your error tracking system like Sentry or backend
    crashlyticsService?.recordError(err.exception, err.stackTrace);

    super.onError(err);
  }

  @override
  void onException(TalkerException exception) {
    /// Send Exception to your error tracking system like Sentry or backend
    super.onException(exception);
  }

  @override
  void onLog(TalkerDataInterface log) {
    /// Send log message to Grafana or backend
    crashlyticsService?.log(log.displayMessage);

    super.onLog(log);
  }
}

Future<void> bootstrap(
  Widget Function() builder, {
  String environment = Environment.dev,
}) async {
  final widgetsBinding = WidgetsFlutterBinding.ensureInitialized();

  // Keep splash open
  FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);

  await Hive.initFlutter();
  Hive.registerAdapter(RandomBeerEntityAdapter());
  // setup GetIt
  await configureDependencies(environment: environment);

  // Prevent rotating screen
  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  // initialize localization
  await EasyLocalization.ensureInitialized();

  // setup logger
  /* TODO uncomment if firebase is registered
  //final CrashlyticsService crashlyticsService = resolve();

  logger.configure(
    settings: TalkerSettings(
      enabled: Platform.I.buildMode == BuildMode.debug,
    ),
    observer: CrashlyticsTalkerObserver(crashlyticsService),
  );*/

  // display logs with global logger, send logs to crashlytics
  // display logs with global logger, send logs to crashlytics
  FlutterError.demangleStackTrace = (StackTrace stack) {
    if (stack is stack_trace.Trace) return stack.vmTrace;
    if (stack is stack_trace.Chain) return stack.toTrace().vmTrace;

    return stack;
  };

  FlutterError.onError = (details) {
    FlutterError.presentError(details);
    //crashlyticsService.recordFlutterFatalError(details);
    logger.error(
      details.exceptionAsString(),
      details.exception,
      details.stack,
    );
  };
  PlatformDispatcher.instance.onError = (error, stack) {
    //crashlyticsService.recordError(error, stack);
    logger.error('PlatformDispatcher', error, stack);

    return true;
  };

  // stop verbose easy_localization logs
  EasyLocalization.logger.enableLevels = [
    LevelMessages.error,
    LevelMessages.warning,
  ];

  // setup bloc observer
  Bloc.observer = TalkerBlocObserver(talker: logger);

  FlutterNativeSplash.remove();

  //flutter pub run easy_localization:generate -f keys --source-dir 'assets/translations' -O 'lib/gen' -o locale_keys.g.dart
  runApp(
    EasyLocalization(
      supportedLocales: const [Locale('en')],
      path: 'assets/translations',
      fallbackLocale: const Locale('en'),
      assetLoader: const CodegenLoader(),
      child: BlocProvider(
        create: (BuildContext context) => AppCubitState(),
        child: Builder(
          builder: (context) {
            return BlocBuilder<AppCubitState, AppState>(
              builder: (context, state) {
                return state.isLoading
                    ? const Material(
                        child: CircularProgressIndicator.adaptive(),
                      )
                    : builder();
              },
            );
          },
        ),
      ),
    ),
  );
}
