import 'package:injectable/injectable.dart';

import 'package:supercharge_ff_kristof/app/core/clients/api.dart';
import 'package:supercharge_ff_kristof/app/core/common_models/beer_model.dart';
import 'package:supercharge_ff_kristof/app/core/constants/app_constants.dart';
import 'package:supercharge_ff_kristof/features/detail_beer/domain/repositories/single_beer_repository.dart';
import 'package:supercharge_ff_kristof/features/random/domain/repositories/random_beer_repository.dart';

@Environment(Environment.dev)
@LazySingleton(as: ISingleBeerRepository)
class SingleBeerRepositoryMock implements ISingleBeerRepository {
  SingleBeerRepositoryMock();

  @override
  Future<BeerModel> getBeer(int id) async {
    try {
      //TODO this pagination not working properly in my opinion, we need to work with list of data to prefetch
      await Future.delayed(const Duration(seconds: 1));
      return Future.value(BeerModel.mock);
    } catch (error) {
      rethrow;
    }
  }
}
