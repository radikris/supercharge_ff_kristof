import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';

import 'package:supercharge_ff_kristof/app/core/clients/api.dart';
import 'package:supercharge_ff_kristof/app/core/common_models/beer_model.dart';
import 'package:supercharge_ff_kristof/app/core/constants/app_constants.dart';
import 'package:supercharge_ff_kristof/features/detail_beer/domain/repositories/single_beer_repository.dart';
import 'package:supercharge_ff_kristof/features/random/domain/repositories/random_beer_repository.dart';

@Environment(Environment.prod)
@LazySingleton(as: ISingleBeerRepository)
class SingleBeerRepositoryImpl implements ISingleBeerRepository {
  SingleBeerRepositoryImpl(this._api);

  final ApiClient _api;

  @override
  Future<BeerModel> getBeer(int id) async {
    try {
      await Future.delayed(const Duration(seconds: 2));

      final results = await _api.getSingleBeer(id);

      return results[0];
    } catch (error) {
      debugPrint(error.toString());

      rethrow;
    }
  }
}
