import 'dart:ui';

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:rxdart/rxdart.dart';
import 'package:skeletonizer/skeletonizer.dart';
import 'package:supercharge_ff_kristof/app/core/common_models/beer_model.dart';
import 'package:supercharge_ff_kristof/app/core/constants/app_dimen.dart';
import 'package:supercharge_ff_kristof/app/core/state/app_cubit_state.dart';
import 'package:supercharge_ff_kristof/app/core/widgets/app_fade_image.dart';
import 'package:supercharge_ff_kristof/app/injection/injection.dart';
import 'package:supercharge_ff_kristof/features/detail_beer/domain/entities/detail_beer_entity.dart';
import 'package:supercharge_ff_kristof/features/detail_beer/domain/usecases/single_beer_usecase.dart';
import 'package:supercharge_ff_kristof/features/random/domain/entities/random_beer_entity.dart';

@RoutePage<void>()
class BeerDetailPage extends StatelessWidget {
  const BeerDetailPage({super.key, required this.beer});
  final RandomBeerEntity beer;

  @override
  Widget build(BuildContext context) {
    return BeerDetailView(
      beer: beer,
    );
  }
}

class BeerDetailView extends StatefulWidget {
  const BeerDetailView({super.key, required this.beer});
  final RandomBeerEntity beer;

  @override
  State<BeerDetailView> createState() => _BeerDetailViewState();
}

class _BeerDetailViewState extends State<BeerDetailView> {
  late final BehaviorSubject<DetailBeerEntity> _beerStreamController;

  @override
  void initState() {
    super.initState();
    _beerStreamController = BehaviorSubject<DetailBeerEntity>();
    _fetchBeer();
  }

  Future<void> _fetchBeer() async {
    try {
      final result = await resolve<SingleBeerUseCase>().call(widget.beer.id!);
      result.when(
        success: (success) {
          _beerStreamController.add(success);
        },
        error: (error) {
          context.read<AppCubitState>().setError(error.toString());
        },
      );
    } catch (error) {
      // Handle error
      context.read<AppCubitState>().setError(error.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Scaffold(
        body: SafeArea(
          child: CustomScrollView(
            slivers: [
              SliverAppBar(
                backgroundColor: Colors.white,
                foregroundColor: Colors.black,
                expandedHeight: 250,
                pinned: true,
                flexibleSpace: FlexibleSpaceBar(
                  centerTitle: true,
                  title: Text(
                    widget.beer.name ?? '',
                    style: TextStyle(color: context.theme.primaryColor),
                  ),
                  background: Hero(
                    tag: widget.beer.id.toString(),
                    child: AppFadeImage(
                      image: widget.beer.image ?? '',
                      height: 250,
                      width: 100,
                    ),
                  ),
                ),
              ),
              SliverToBoxAdapter(
                child: Padding(
                  padding: AppDimen.edge16,
                  child: Column(
                    children: [
                      const SizedBox(
                        height: 16,
                      ),
                      Text(
                        widget.beer.name ?? '',
                        style: context.textTheme.titleLarge,
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                      Text(
                        widget.beer.tagline ?? '',
                        style: context.textTheme.bodySmall,
                      ),
                      const SizedBox(
                        height: 64,
                      ),
                    ],
                  ),
                ),
              ),
              SliverToBoxAdapter(
                child: Padding(
                  padding: AppDimen.edgeh32,
                  child: StreamBuilder<DetailBeerEntity>(
                    stream: _beerStreamController.stream,
                    builder: (context, snapshot) {
                      final beer = snapshot.hasData
                          ? snapshot.data!
                          : DetailBeerEntity.fromModel(BeerModel.mock);
                      return Skeletonizer(
                        enabled:
                            snapshot.connectionState == ConnectionState.waiting,
                        child: ListView(
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(),
                          children: [
                            const SizedBox(
                              height: 16,
                            ),

                            Text('Description: ${beer.description ?? 'N/A'}'),
                            SizedBox(
                              height: AppDimen.h8,
                            ),
                            Text('Image: ${beer.image ?? 'N/A'}'),
                            SizedBox(
                              height: AppDimen.h8,
                            ),

                            Text('ABV: ${beer.abv ?? 'N/A'}'),
                            SizedBox(
                              height: AppDimen.h8,
                            ),

                            Text('IBU: ${beer.ibu ?? 'N/A'}'),
                            SizedBox(
                              height: AppDimen.h8,
                            ),

                            Text('Target FG: ${beer.targetFg ?? 'N/A'}'),
                            SizedBox(
                              height: AppDimen.h8,
                            ),

                            Text('Target OG: ${beer.targetOg ?? 'N/A'}'),
                            SizedBox(
                              height: AppDimen.h8,
                            ),

                            Text('EBC: ${beer.ebc ?? 'N/A'}'),
                            SizedBox(
                              height: AppDimen.h8,
                            ),

                            Text('SRM: ${beer.srm ?? 'N/A'}'),
                            SizedBox(
                              height: AppDimen.h8,
                            ),

                            Text('pH: ${beer.ph ?? 'N/A'}'),
                            SizedBox(
                              height: AppDimen.h8,
                            ),

                            Text(
                              'Attenuation Level: ${beer.attenuationLevel ?? 'N/A'}',
                            ),
                            // Add more properties as needed
                          ],
                        ),
                      );
                    },
                  ),
                ),
              ),
              /*   SliverToBoxAdapter(
                child: FutureBuilder<Object>(
                  builder: (context, snapshot) {
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                         SizedBox(
                          height: 16,
                        ),
                        Text('Name: ${beer.name ?? 'N/A'}'),
                        Text('Tagline: ${beer.tagline ?? 'N/A'}'),
                        Text('Description: ${beer.description ?? 'N/A'}'),
                        Text('Image: ${beer.image ?? 'N/A'}'),
                        Text('ABV: ${beer.abv ?? 'N/A'}'),
                        Text('IBU: ${beer.ibu ?? 'N/A'}'),
                        Text('Target FG: ${beer.targetFg ?? 'N/A'}'),
                        Text('Target OG: ${beer.targetOg ?? 'N/A'}'),
                        Text('EBC: ${beer.ebc ?? 'N/A'}'),
                        Text('SRM: ${beer.srm ?? 'N/A'}'),
                        Text('pH: ${beer.ph ?? 'N/A'}'),
                        Text(
                            'Attenuation Level: ${beer.attenuationLevel ?? 'N/A'}'),
                        // Add more properties as needed
                      ],
                    );
                  }, future: getIt<SingleBeerUse>().call(),
                ),
              ), */
            ],
          ),
        ),
      ),
    );
  }
}
