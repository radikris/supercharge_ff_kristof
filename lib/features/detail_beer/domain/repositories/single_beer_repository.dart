import 'package:supercharge_ff_kristof/app/core/common_models/beer_model.dart';

abstract class ISingleBeerRepository {
  Future<BeerModel> getBeer(int id);
}
