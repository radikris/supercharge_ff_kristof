import 'package:hive/hive.dart';
import 'package:supercharge_ff_kristof/app/core/common_models/beer_model.dart';

@HiveType(typeId: 1)
class DetailBeerEntity {
  DetailBeerEntity({
    required this.id,
    required this.name,
    required this.tagline,
    required this.description,
    required this.image,
    required this.abv,
    required this.ibu,
    required this.targetFg,
    required this.targetOg,
    required this.ebc,
    required this.srm,
    required this.ph,
    required this.attenuationLevel,
  });

  factory DetailBeerEntity.fromModel(BeerModel model) {
    return DetailBeerEntity(
      id: model.id,
      name: model.name,
      tagline: model.tagline,
      description: model.description,
      image: model.imageUrl,
      abv: model.abv,
      ibu: model.ibu,
      targetFg: model.targetFg,
      targetOg: model.targetOg,
      ebc: model.ebc,
      srm: model.srm,
      ph: model.ph,
      attenuationLevel: model.attenuationLevel,
    );
  }

  final int? id;
  final String? name;
  final String? tagline;
  final String? description;
  final String? image;
  final double? abv;
  final double? ibu;
  final double? targetFg;
  final double? targetOg;
  final double? ebc;
  final double? srm;
  final double? ph;
  final double? attenuationLevel;

  // Add other properties as needed

  /*  BeerModel toModel() {
    return BeerModel(
      id: id,
      name: name,
      tagline: tagline,
      description: description,
      imageUrl: image,
      // Set other properties to default values or provide default values if needed
      abv: 0.0,
      ibu: 0.0,
      // ... other properties
    );
  } */
}
