import 'package:injectable/injectable.dart';
import 'package:supercharge_ff_kristof/app/core/clients/network/api_error.dart';
import 'package:supercharge_ff_kristof/app/core/clients/network/api_result.dart';
import 'package:supercharge_ff_kristof/features/detail_beer/domain/entities/detail_beer_entity.dart';
import 'package:supercharge_ff_kristof/features/detail_beer/domain/repositories/single_beer_repository.dart';
import 'package:supercharge_ff_kristof/features/random/domain/entities/random_beer_entity.dart';

@LazySingleton()
class SingleBeerUseCase {
  SingleBeerUseCase(this._singleBeerRepository);
  final ISingleBeerRepository _singleBeerRepository;

  Future<ApiResult<DetailBeerEntity>> call(int id) async {
    try {
      final result = await _singleBeerRepository.getBeer(id);

      return ApiResult.success(DetailBeerEntity.fromModel(result));
    } catch (error) {
      return ApiResult.error(ApiError.fromDioException(error.toString()));
    }
  }
}
