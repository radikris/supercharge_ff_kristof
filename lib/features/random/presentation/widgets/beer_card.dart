import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:supercharge_ff_kristof/app/core/constants/app_dimen.dart';
import 'package:supercharge_ff_kristof/app/core/extensions/context_extensions.dart';
import 'package:supercharge_ff_kristof/app/core/widgets/app_fade_image.dart';
import 'package:supercharge_ff_kristof/app/router/app_router.gr.dart';
import 'package:supercharge_ff_kristof/features/random/domain/entities/random_beer_entity.dart';

class BeerCard extends StatelessWidget {
  const BeerCard({
    super.key,
    required this.beer,
    required this.index,
    required this.onButtonClick,
  });
  final RandomBeerEntity beer;
  final int index;
  final Function(bool) onButtonClick;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        context.router.push(
          BeerDetailRoute(
            beer: beer,
          ),
        );
      },
      child: Card(
        color: Colors.grey.shade200,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(AppDimen.r16),
        ),
        child: SizedBox(
          width: context.width,
          child: Column(
            children: [
              const SizedBox(
                height: 4,
              ),
              Expanded(
                child: AppFadeImage(
                  image: beer.image ?? '',
                  width: AppDimen.w80,
                ),
              ),
              Expanded(
                flex: 2,
                child: Container(
                  margin: AppDimen.edge16,
                  padding: AppDimen.edge16,
                  width: context.width,
                  decoration: BoxDecoration(
                    boxShadow: kElevationToShadow[1],
                    borderRadius: BorderRadius.circular(AppDimen.r10),
                    color: Colors.white,
                  ),
                  child: Column(
                    children: [
                      SizedBox(height: AppDimen.r16),
                      Text(
                        beer.name ?? '',
                        style: context.textTheme.titleLarge,
                      ),
                      SizedBox(height: AppDimen.r16),
                      Container(
                        child: Text(
                          beer.tagline!.toUpperCase(),
                          style: context.textTheme.bodySmall,
                        ),
                      ),
                      const SizedBox(
                        height: 32,
                      ),
                      Text(
                        beer.description ?? '',
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                        style: context.textTheme.bodyMedium,
                      ),
                      SizedBox(height: AppDimen.r16),
                      const Spacer(),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          IconButton(
                            icon: Text(
                              '🚫',
                              style: TextStyle(
                                fontSize: AppDimen.h32,
                              ),
                            ),
                            style: TextButton.styleFrom(
                              backgroundColor: Colors.redAccent,
                            ),
                            onPressed: () {
                              onButtonClick(false);
                            },
                          ),
                          IconButton(
                            icon: Text(
                              '🍺',
                              style: TextStyle(
                                fontSize: AppDimen.h32,
                              ),
                            ),
                            style: TextButton.styleFrom(
                              backgroundColor: Colors.redAccent,
                            ),
                            onPressed: () {
                              onButtonClick(true);
                            },
                          ),
                        ],
                      ),
                      SizedBox(height: AppDimen.r4),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
