part of 'random_beer_bloc.dart';

abstract class RandomBeerEvent extends Equatable {
  const RandomBeerEvent();

  @override
  List<Object> get props => [];
}

class FetchRandomBeerEvent extends RandomBeerEvent {
  /// {@macro custom_my_orders_event}
  const FetchRandomBeerEvent();

  @override
  List<Object> get props => [];
}

class SwipeRandomBeerEvent extends RandomBeerEvent {
  const SwipeRandomBeerEvent(
      {required this.index, required this.save, required this.beer,});
  final bool? save;
  final RandomBeerEntity? beer;
  final int? index;

  @override
  List<Object> get props => [];
}
