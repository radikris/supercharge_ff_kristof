import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:supercharge_ff_kristof/app/core/constants/app_constants.dart';
import 'package:supercharge_ff_kristof/app/core/extensions/api_error_extension.dart';
import 'package:supercharge_ff_kristof/app/core/state/app_cubit_state.dart';
import 'package:supercharge_ff_kristof/features/random/domain/entities/random_beer_entity.dart';

import 'package:equatable/equatable.dart';
import 'package:injectable/injectable.dart';
import 'package:supercharge_ff_kristof/features/random/domain/repositories/random_beer_repository.dart';
import 'package:supercharge_ff_kristof/features/random/domain/usecases/random_beer_usecase.dart';
import 'package:supercharge_ff_kristof/features/random/domain/usecases/save_beer_usecase.dart';

part 'random_beer_event.dart';
part 'random_beer_state.dart';

@injectable
class RandomBeerBloc extends Bloc<RandomBeerEvent, RandomBeerState> {
  RandomBeerBloc({
    required this.saveBeerUseCase,
    required this.randomBeerUseCase,
  }) : super(
          RandomBeerState(
            [],
          ),
        ) {
    on<FetchRandomBeerEvent>(_onFetch);
    on<SwipeRandomBeerEvent>(_onSave);
  }
  final RandomBeerUseCase randomBeerUseCase;
  final SaveBeerUseCase saveBeerUseCase;

  FutureOr<void> _onFetch(
    FetchRandomBeerEvent event,
    Emitter<RandomBeerState> emit,
  ) async {
    final result = await randomBeerUseCase();

    result.when(
      success: (success) {
        final stateBeersAndNewBeers = [...state.beers, ...success];
        emit(
          state.copyWith(
            beers: stateBeersAndNewBeers,
          ),
        );
      },
      error: (error) {
        emit(state.copyWith(error: error.errorMessage));
      },
    );
  }

  FutureOr<void> _onSave(
    SwipeRandomBeerEvent event,
    Emitter<RandomBeerState> emit,
  ) async {
    if (event.save == true) {
      final result = await saveBeerUseCase(event.beer!);
    }
    final currentBeers = state.beers;
    if (event.index! % AppConstants.PRELOAD_PAGE == 0) {
      final result = add(const FetchRandomBeerEvent());
    }
    emit(
      state.copyWith(
        beers: currentBeers,
      ),
    );
  }
}
