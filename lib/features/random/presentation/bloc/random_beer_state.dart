// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'random_beer_bloc.dart';

class RandomBeerState {
  List<RandomBeerEntity> beers;
  String? error;

  RandomBeerState(this.beers, {this.error});

  RandomBeerState copyWith({
    List<RandomBeerEntity>? beers,
    String? error,
  }) {
    return RandomBeerState(beers ?? [], error: error);
  }
}
