import 'package:appinio_swiper/appinio_swiper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';

import 'package:auto_route/auto_route.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:supercharge_ff_kristof/app/core/constants/app_constants.dart';
import 'package:supercharge_ff_kristof/app/core/constants/app_dimen.dart';
import 'package:supercharge_ff_kristof/app/core/extensions/context_extensions.dart';
import 'package:supercharge_ff_kristof/app/core/state/app_cubit_state.dart';
import 'package:supercharge_ff_kristof/app/injection/injection.dart';
import 'package:supercharge_ff_kristof/app/router/app_router.gr.dart';
import 'package:supercharge_ff_kristof/features/random/presentation/bloc/random_beer_bloc.dart';
import 'package:supercharge_ff_kristof/features/random/presentation/widgets/beer_card.dart';
import 'package:supercharge_ff_kristof/gen/locale_keys.g.dart';

@RoutePage<void>()
class RandomBeerPage extends StatelessWidget {
  const RandomBeerPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<RandomBeerBloc>(
      create: (context) =>
          resolve<RandomBeerBloc>()..add(const FetchRandomBeerEvent()),
      child: const RandomBeerView(),
    );
  }
}

@RoutePage<void>()
class RandomBeerView extends StatefulWidget {
  const RandomBeerView({super.key});

  @override
  State<RandomBeerView> createState() => _RandomBeerViewState();
}

class _RandomBeerViewState extends State<RandomBeerView> {
  int counter = 0;

  void incrementCounter() {
    setState(() {
      counter++;
      if (counter == 10) {
        context.router.replace(const ListBeerRoute());
      }
    });
  }

  final AppinioSwiperController controller = AppinioSwiperController();

  @override
  Widget build(BuildContext context) {
    final state = context.watch<RandomBeerBloc>().state;

    return BlocListener<RandomBeerBloc, RandomBeerState>(
      listener: (context, state) {
        //set global or local loading or error
        if (state.error != null) {
          context.read<AppCubitState>().setError(state.error!);
        }
      },
      child: CupertinoPageScaffold(
        child: SafeArea(
          child: Column(
            children: [
              Text(
                'Lets check your beer taste!🍻',
                style: context.textTheme.bodyLarge,
              ),
              const SizedBox(
                height: 4,
              ),
              Text(
                'We will show you ${AppConstants.BEERS_MAX} random beers.',
                style: context.textTheme.bodySmall,
              ),
              SizedBox(
                height: AppDimen.h8,
              ),
              Stack(
                children: [
                  Container(
                    color: context.theme.hintColor,
                    width: context.width * 0.8,
                    height: 10,
                  ),
                  AnimatedContainer(
                    duration: Duration(
                      milliseconds: AppConstants.ANIMATION_DURATION,
                    ),
                    color: context.theme.primaryColor,
                    height: 10,
                    width: ((context.width * 0.8) / AppConstants.BEERS_MAX) *
                        counter,
                  ),
                ],
              ),
              if (state.beers.isEmpty)
                SizedBox(
                  height: context.height * 0.75,
                  child: const Center(
                    child: CircularProgressIndicator(),
                  ),
                )
              else
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.8,
                  child: AppinioSwiper(
                    controller: controller,
                    cardsCount: AppConstants.BEERS_MAX,
                    swipeOptions: const AppinioSwipeOptions.only(
                      left: true,
                      right: true,
                    ),
                    onSwipe: (index, direction) {
                      incrementCounter();
                      context.read<RandomBeerBloc>().add(
                            SwipeRandomBeerEvent(
                              index: index,
                              save: direction == AppinioSwiperDirection.right,
                              beer: state.beers[index],
                            ),
                          );
                    },
                    cardsBuilder: (BuildContext context, int index) {
                      return Container(
                        alignment: Alignment.center,
                        child: BeerCard(
                          onButtonClick: (bool save) {
                            if (save) {
                              controller.swipeRight();
                            } else {
                              controller.swipeLeft();
                            }
                          },
                          index: 0,
                          beer: state.beers[index],
                        ),
                      );
                    },
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }
}
