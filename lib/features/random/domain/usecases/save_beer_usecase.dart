import 'package:hive_flutter/hive_flutter.dart';
import 'package:injectable/injectable.dart';
import 'package:supercharge_ff_kristof/app/core/clients/network/api_error.dart';
import 'package:supercharge_ff_kristof/app/core/clients/network/api_result.dart';
import 'package:supercharge_ff_kristof/features/random/domain/entities/random_beer_entity.dart';
import 'package:supercharge_ff_kristof/features/random/domain/repositories/local_storage_repository.dart';
import 'package:supercharge_ff_kristof/features/random/domain/repositories/random_beer_repository.dart';

/* @LazySingleton()
class SaveBeerUseCase {
  SaveBeerUseCase(this._randomBeerBox);
  final Box<dynamic> _randomBeerBox;

  Future<void> call(RandomBeerEntity beer) async {
    await _randomBeerBox.put(beer.id, beer);
    final box = Hive.openBox('test');
  }
}
 */

@LazySingleton()
class SaveBeerUseCase {
  SaveBeerUseCase(this.localStorageRepository);
  ILocalStorageRepository localStorageRepository;
  Future<void> call(RandomBeerEntity beer) async {
    //final box = Hive.box('random_beer');
    try {
      final box = await localStorageRepository.openBox();
      await localStorageRepository.addBeer(box, beer);
    } catch (e) {
      rethrow;
    }
  }
}
