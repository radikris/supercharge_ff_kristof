import 'package:hive_flutter/hive_flutter.dart';
import 'package:injectable/injectable.dart';
import 'package:supercharge_ff_kristof/app/core/clients/network/api_error.dart';
import 'package:supercharge_ff_kristof/app/core/clients/network/api_result.dart';
import 'package:supercharge_ff_kristof/features/random/domain/entities/random_beer_entity.dart';
import 'package:supercharge_ff_kristof/features/random/domain/repositories/random_beer_repository.dart';

@LazySingleton()
class RandomBeerUseCase {
  RandomBeerUseCase(this._randomBeerRepository);
  final IRandomBeerRepository _randomBeerRepository;

  Future<ApiResult<List<RandomBeerEntity>>> call() async {
    try {
      final results = await _randomBeerRepository.getRandomBeer();
      final randomBeerEntityList =
          results.map(RandomBeerEntity.fromModel).toList();

      return ApiResult.success(randomBeerEntityList);
    } catch (error) {
      return ApiResult.error(ApiError.fromDioException(error.toString()));
    }
  }
}
