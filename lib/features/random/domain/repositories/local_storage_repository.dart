import 'package:hive/hive.dart';
import 'package:supercharge_ff_kristof/features/random/domain/entities/random_beer_entity.dart';

abstract class ILocalStorageRepository {
  Future<Box> openBox();
  List<RandomBeerEntity> getBeers(Box box);
  Future<void> addBeer(Box box, RandomBeerEntity product);
  Future<void> removeBeer(Box box, RandomBeerEntity product);
  Future<void> clearBeers(Box box);
}
