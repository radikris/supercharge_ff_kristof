import 'package:supercharge_ff_kristof/app/core/common_models/beer_model.dart';

abstract class IRandomBeerRepository {
  Future<List<BeerModel>> getRandomBeer();
}
