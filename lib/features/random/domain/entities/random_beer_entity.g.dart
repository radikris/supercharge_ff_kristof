// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'random_beer_entity.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class RandomBeerEntityAdapter extends TypeAdapter<RandomBeerEntity> {
  @override
  final int typeId = 1;

  @override
  RandomBeerEntity read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return RandomBeerEntity(
      id: fields[0] as int?,
      name: fields[1] as String?,
      tagline: fields[2] as String?,
      description: fields[3] as String?,
      image: fields[4] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, RandomBeerEntity obj) {
    writer
      ..writeByte(5)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.name)
      ..writeByte(2)
      ..write(obj.tagline)
      ..writeByte(3)
      ..write(obj.description)
      ..writeByte(4)
      ..write(obj.image);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is RandomBeerEntityAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
