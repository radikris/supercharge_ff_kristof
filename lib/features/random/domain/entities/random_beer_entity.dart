import 'package:hive/hive.dart';
import 'package:supercharge_ff_kristof/app/core/common_models/beer_model.dart';

part 'random_beer_entity.g.dart';

@HiveType(typeId: 1)
class RandomBeerEntity {
  RandomBeerEntity({
    required this.id,
    required this.name,
    required this.tagline,
    required this.description,
    required this.image,
  });

  factory RandomBeerEntity.fromModel(BeerModel model) {
    return RandomBeerEntity(
      id: model.id,
      name: model.name,
      tagline: model.tagline,
      description: model.description,
      image: model.imageUrl,
    );
  }
  @HiveField(0)
  final int? id;
  @HiveField(1)
  final String? name;
  @HiveField(2)
  final String? tagline;
  @HiveField(3)
  final String? description;
  @HiveField(4)
  final String? image;

  /*  BeerModel toModel() {
    return BeerModel(
      id: id,
      name: name,
      tagline: tagline,
      description: description,
      imageUrl: image,
      // Set other properties to default values or provide default values if needed
      abv: 0.0,
      ibu: 0.0,
      // ... other properties
    );
  } */
}
