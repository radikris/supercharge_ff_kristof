import 'package:injectable/injectable.dart';

import 'package:supercharge_ff_kristof/app/core/clients/api.dart';
import 'package:supercharge_ff_kristof/app/core/common_models/beer_model.dart';
import 'package:supercharge_ff_kristof/app/core/constants/app_constants.dart';
import 'package:supercharge_ff_kristof/features/random/domain/repositories/random_beer_repository.dart';

@Environment(Environment.dev)
@LazySingleton(as: IRandomBeerRepository)
class RandomBeerRepositoryMock implements IRandomBeerRepository {
  RandomBeerRepositoryMock();

  @override
  Future<List<BeerModel>> getRandomBeer() async {
    try {
      //TODO this pagination not working properly in my opinion, we need to work with list of data to prefetch
      final results = await Future.wait(
        List.generate(
            AppConstants.PAGE_SIZE, (_) => Future.value(BeerModel.mock)),
      );

      return results;
    } catch (error) {
      rethrow;
    }
  }
}
