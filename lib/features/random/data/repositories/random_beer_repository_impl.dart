import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';

import 'package:supercharge_ff_kristof/app/core/clients/api.dart';
import 'package:supercharge_ff_kristof/app/core/common_models/beer_model.dart';
import 'package:supercharge_ff_kristof/app/core/constants/app_constants.dart';
import 'package:supercharge_ff_kristof/features/random/domain/repositories/random_beer_repository.dart';

@Environment(Environment.prod)
@LazySingleton(as: IRandomBeerRepository)
class RandomBeerRepositoryImpl implements IRandomBeerRepository {
  RandomBeerRepositoryImpl(this._api);

  final ApiClient _api;

  @override
  Future<List<BeerModel>> getRandomBeer() async {
    try {
      //TODO this pagination not working properly in my opinion, we need to work with list of data to prefetch
      final results = await Future.wait(
        List.generate(
          AppConstants.PAGE_SIZE,
          (_) => _api.getRandomBeer().then((value) {
            print(value.first.toJson());
            return value.first;
          }),
        ),
      );

      return results;
    } catch (error) {
      debugPrint(error.toString());

      rethrow;
    }
  }
}
