import 'package:hive/hive.dart';
import 'package:injectable/injectable.dart';
import 'package:supercharge_ff_kristof/features/random/domain/entities/random_beer_entity.dart';
import 'package:supercharge_ff_kristof/features/random/domain/repositories/local_storage_repository.dart';

@LazySingleton(as: ILocalStorageRepository)
class LocalStorageRepository extends ILocalStorageRepository {
  String boxName = 'beers';
  Type boxType = RandomBeerEntity;

  @override
  Future<Box> openBox() async {
    final Box box = await Hive.openBox<RandomBeerEntity>(boxName);
    return box;
  }

  @override
  List<RandomBeerEntity> getBeers(Box box) {
    return box.values.toList() as List<RandomBeerEntity>;
  }

  @override
  Future<void> addBeer(Box box, RandomBeerEntity beer) async {
    await box.put(beer.id, beer);
  }

  @override
  Future<void> removeBeer(
    Box box,
    RandomBeerEntity beer,
  ) async {
    await box.delete(beer.id);
  }

  @override
  Future<void> clearBeers(Box box) async {
    await box.clear();
  }
}
