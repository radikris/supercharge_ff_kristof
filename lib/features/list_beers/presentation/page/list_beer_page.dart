import 'package:appinio_swiper/appinio_swiper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';

import 'package:auto_route/auto_route.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lottie/lottie.dart';
import 'package:skeletonizer/skeletonizer.dart';
import 'package:supercharge_ff_kristof/app/core/common_models/beer_model.dart';
import 'package:supercharge_ff_kristof/app/core/extensions/context_extensions.dart';
import 'package:supercharge_ff_kristof/app/core/widgets/app_fade_image.dart';
import 'package:supercharge_ff_kristof/app/injection/injection.dart';
import 'package:supercharge_ff_kristof/app/router/app_router.gr.dart';
import 'package:supercharge_ff_kristof/features/list_beers/presentation/bloc/saved_beer_bloc.dart';
import 'package:supercharge_ff_kristof/features/random/domain/entities/random_beer_entity.dart';
import 'package:supercharge_ff_kristof/features/random/presentation/bloc/random_beer_bloc.dart';
import 'package:supercharge_ff_kristof/features/random/presentation/widgets/beer_card.dart';
import 'package:supercharge_ff_kristof/gen/assets.gen.dart';
import 'package:supercharge_ff_kristof/gen/locale_keys.g.dart';

@RoutePage<void>()
class ListBeerPage extends StatelessWidget {
  const ListBeerPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<SavedBeerBloc>(
      create: (context) =>
          resolve<SavedBeerBloc>()..add(const FetchSavedBeerEvent()),
      child: const ListBeerView(),
    );
  }
}

class ListBeerView extends StatelessWidget {
  const ListBeerView({super.key});

  @override
  Widget build(BuildContext context) {
    final state = context.watch<SavedBeerBloc>().state;
    final bloc = context.watch<SavedBeerBloc>();
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Saved beers'),
        actions: [
          IconButton(
            onPressed: () {
              context.router.replace(const RandomBeerRoute());
            },
            icon: const Icon(Icons.add),
          ),
          IconButton(
            onPressed: () {
              bloc.add(const ClearSavedBeerEvent());
            },
            icon: const Icon(Icons.no_drinks),
          ),
        ],
      ),
      body: state.empty
          ? Column(
              children: [
                Lottie.asset(Assets.illustration.notfound),
                const Text('No beers saved yet'),
              ],
            )
          : Material(
              child: ListView.builder(
                itemCount: state.beers.isEmpty ? 10 : state.beers.length,
                itemBuilder: (BuildContext context, int index) {
                  final beer = state.beers.isEmpty
                      ? RandomBeerEntity.fromModel(BeerModel.mock)
                      : state.beers[index];

                  return Skeletonizer(
                    enabled: state.beers.isEmpty,
                    child: GestureDetector(
                      onTap: () {
                        context.router.push(
                          BeerDetailRoute(
                            beer: beer,
                          ),
                        );
                      },
                      child: Hero(
                        tag: beer.id.toString(),
                        child: Material(
                          child: ListTile(
                            leading: CircleAvatar(
                              backgroundColor: Colors.grey.shade200,
                              radius: 30,
                              child:
                                  beer.image != null && state.beers.isNotEmpty
                                      ? Image.network(
                                          beer.image!,
                                        )
                                      : const SizedBox(),
                            ),
                            title: Text(beer.name ?? ''),
                            subtitle: Text(beer.tagline?.toUpperCase() ?? ''),
                          ),
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
    );
  }
}
