// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'saved_beer_bloc.dart';

class SavedBeerState {
  List<RandomBeerEntity> beers;
  bool empty;

  SavedBeerState(this.beers, this.empty);

  SavedBeerState copyWith({List<RandomBeerEntity>? beers, bool? empty}) {
    return SavedBeerState(
      beers ?? [],
      empty ?? false,
    );
  }
}
