import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:supercharge_ff_kristof/app/core/constants/app_constants.dart';
import 'package:supercharge_ff_kristof/app/core/state/app_cubit_state.dart';
import 'package:supercharge_ff_kristof/features/list_beers/domain/usecases/clear_saved_beer_usecase.dart';
import 'package:supercharge_ff_kristof/features/list_beers/domain/usecases/read_saved_beer_usecase.dart';
import 'package:supercharge_ff_kristof/features/random/domain/entities/random_beer_entity.dart';

import 'package:equatable/equatable.dart';
import 'package:injectable/injectable.dart';

part 'saved_beer_event.dart';
part 'saved_beer_state.dart';

@injectable
class SavedBeerBloc extends Bloc<SavedBeerEvent, SavedBeerState> {
  SavedBeerBloc({
    required this.savedBeerUseCase,
    required this.appState,
    required this.clearSavedBeerUseCase,
  }) : super(
          SavedBeerState(
            [],
            false,
          ),
        ) {
    on<FetchSavedBeerEvent>(_onFetch);
    on<ClearSavedBeerEvent>(_onDelete);
  }
  final ReadSavedBeerUseCase savedBeerUseCase;
  final ClearSavedBeerUseCase clearSavedBeerUseCase;
  final AppCubitState appState;

  FutureOr<void> _onFetch(
    FetchSavedBeerEvent event,
    Emitter<SavedBeerState> emit,
  ) async {
    await Future.delayed(
      const Duration(seconds: 2),
    ); //TODO REMOVE JUST ANIMATION PURPOSES
    final result = await savedBeerUseCase();

    emit(state.copyWith(beers: result, empty: result.isEmpty));
  }

  FutureOr<void> _onDelete(
    ClearSavedBeerEvent event,
    Emitter<SavedBeerState> emit,
  ) async {
    final result = await clearSavedBeerUseCase();

    emit(state.copyWith(beers: [], empty: true));
  }
}
