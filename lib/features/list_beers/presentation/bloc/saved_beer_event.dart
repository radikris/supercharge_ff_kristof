part of 'saved_beer_bloc.dart';

abstract class SavedBeerEvent extends Equatable {
  const SavedBeerEvent();

  @override
  List<Object> get props => [];
}

class FetchSavedBeerEvent extends SavedBeerEvent {
  /// {@macro custom_my_orders_event}
  const FetchSavedBeerEvent();

  @override
  List<Object> get props => [];
}

class ClearSavedBeerEvent extends SavedBeerEvent {
  /// {@macro custom_my_orders_event}
  const ClearSavedBeerEvent();

  @override
  List<Object> get props => [];
}
