import 'package:injectable/injectable.dart';
import 'package:supercharge_ff_kristof/features/random/domain/entities/random_beer_entity.dart';
import 'package:supercharge_ff_kristof/features/random/domain/repositories/local_storage_repository.dart';

@LazySingleton()
class ClearSavedBeerUseCase {
  ClearSavedBeerUseCase(this.localStorageRepository);
  final ILocalStorageRepository localStorageRepository;

  Future<void> call() async {
    try {
      final box = await localStorageRepository.openBox();
      await localStorageRepository.clearBeers(box);
    } catch (e) {
      rethrow;
    }
  }
}
