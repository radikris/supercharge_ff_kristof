import 'package:injectable/injectable.dart';
import 'package:supercharge_ff_kristof/features/random/domain/entities/random_beer_entity.dart';
import 'package:supercharge_ff_kristof/features/random/domain/repositories/local_storage_repository.dart';

@LazySingleton()
class ReadSavedBeerUseCase {
  ReadSavedBeerUseCase(this.localStorageRepository);
  final ILocalStorageRepository localStorageRepository;

  Future<List<RandomBeerEntity>> call() async {
    try {
      final box = await localStorageRepository.openBox();
      final beers = localStorageRepository.getBeers(box);
      return beers;
    } catch (e) {
      rethrow;
    }
  }
}
