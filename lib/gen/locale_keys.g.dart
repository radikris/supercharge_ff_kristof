// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

abstract class  LocaleKeys {
  static const email = 'email';
  static const password = 'password';
  static const counter_appBarTitle = 'counter.appBarTitle';
  static const counter = 'counter';
  static const todo_todokey = 'todo.todokey';
  static const todo = 'todo';
  static const randomTitle = 'randomTitle';
  static const randomSubtitle = 'randomSubtitle';
  static const savedBeers = 'savedBeers';

}
