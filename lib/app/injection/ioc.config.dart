// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:crashlytics_service/crashlytics_service.dart' as _i6;
import 'package:dio/dio.dart' as _i7;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

import '../../features/detail_beer/data/repositories/single_beer_repository_impl.dart'
    as _i21;
import '../../features/detail_beer/data/repositories/single_beer_repository_mock.dart'
    as _i13;
import '../../features/detail_beer/domain/repositories/single_beer_repository.dart'
    as _i12;
import '../../features/detail_beer/domain/usecases/single_beer_usecase.dart'
    as _i17;
import '../../features/list_beers/domain/usecases/clear_saved_beer_usecase.dart'
    as _i19;
import '../../features/list_beers/domain/usecases/read_saved_beer_usecase.dart'
    as _i15;
import '../../features/list_beers/presentation/bloc/saved_beer_bloc.dart'
    as _i23;
import '../../features/random/data/repositories/local_storage_repository.dart'
    as _i9;
import '../../features/random/data/repositories/random_beer_repository_impl.dart'
    as _i20;
import '../../features/random/data/repositories/random_beer_repository_mock.dart'
    as _i11;
import '../../features/random/domain/repositories/local_storage_repository.dart'
    as _i8;
import '../../features/random/domain/repositories/random_beer_repository.dart'
    as _i10;
import '../../features/random/domain/usecases/random_beer_usecase.dart' as _i14;
import '../../features/random/domain/usecases/save_beer_usecase.dart' as _i16;
import '../../features/random/presentation/bloc/random_beer_bloc.dart' as _i22;
import '../core/clients/api.dart' as _i18;
import '../core/clients/network/config.dart' as _i5;
import '../core/clients/network/network_client.dart' as _i26;
import '../core/state/app_cubit_state.dart' as _i3;
import '../router/router.dart' as _i4;
import 'module.dart' as _i24;
import 'service_module.dart' as _i25;

const String _dev = 'dev';
const String _prod = 'prod';

// initializes the registration of main-scope dependencies inside of GetIt
_i1.GetIt init(
  _i1.GetIt getIt, {
  String? environment,
  _i2.EnvironmentFilter? environmentFilter,
}) {
  final gh = _i2.GetItHelper(
    getIt,
    environment,
    environmentFilter,
  );
  final injectableModule = _$InjectableModule();
  final serviceModule = _$ServiceModule();
  final dioInjectableModule = _$DioInjectableModule();
  final registerModule = _$RegisterModule();
  gh.singleton<_i3.AppCubitState>(_i3.AppCubitState());
  gh.lazySingleton<_i4.AppRouter>(() => injectableModule.router);
  gh.singleton<_i5.Config>(
    _i5.ConfigDev(),
    registerFor: {_dev},
  );
  gh.singleton<_i5.Config>(
    _i5.ConfigProd(),
    registerFor: {_prod},
  );
  gh.factory<_i6.CrashlyticsService>(() => serviceModule.crashlyticsService);
  gh.factory<_i7.Dio>(() => dioInjectableModule.getDio(gh<_i5.Config>()));
  gh.lazySingleton<_i8.ILocalStorageRepository>(
      () => _i9.LocalStorageRepository());
  gh.lazySingleton<_i10.IRandomBeerRepository>(
    () => _i11.RandomBeerRepositoryMock(),
    registerFor: {_dev},
  );
  gh.lazySingleton<_i12.ISingleBeerRepository>(
    () => _i13.SingleBeerRepositoryMock(),
    registerFor: {_dev},
  );
  gh.lazySingleton<_i14.RandomBeerUseCase>(
      () => _i14.RandomBeerUseCase(gh<_i10.IRandomBeerRepository>()));
  gh.lazySingleton<_i15.ReadSavedBeerUseCase>(
      () => _i15.ReadSavedBeerUseCase(gh<_i8.ILocalStorageRepository>()));
  gh.lazySingleton<_i16.SaveBeerUseCase>(
      () => _i16.SaveBeerUseCase(gh<_i8.ILocalStorageRepository>()));
  gh.lazySingleton<_i17.SingleBeerUseCase>(
      () => _i17.SingleBeerUseCase(gh<_i12.ISingleBeerRepository>()));
  gh.factory<_i18.ApiClient>(() => registerModule.getService(
        gh<_i7.Dio>(),
        gh<_i5.Config>(),
      ));
  gh.lazySingleton<_i19.ClearSavedBeerUseCase>(
      () => _i19.ClearSavedBeerUseCase(gh<_i8.ILocalStorageRepository>()));
  gh.lazySingleton<_i10.IRandomBeerRepository>(
    () => _i20.RandomBeerRepositoryImpl(gh<_i18.ApiClient>()),
    registerFor: {_prod},
  );
  gh.lazySingleton<_i12.ISingleBeerRepository>(
    () => _i21.SingleBeerRepositoryImpl(gh<_i18.ApiClient>()),
    registerFor: {_prod},
  );
  gh.factory<_i22.RandomBeerBloc>(() => _i22.RandomBeerBloc(
        saveBeerUseCase: gh<_i16.SaveBeerUseCase>(),
        randomBeerUseCase: gh<_i14.RandomBeerUseCase>(),
      ));
  gh.factory<_i23.SavedBeerBloc>(() => _i23.SavedBeerBloc(
        savedBeerUseCase: gh<_i15.ReadSavedBeerUseCase>(),
        appState: gh<_i3.AppCubitState>(),
        clearSavedBeerUseCase: gh<_i19.ClearSavedBeerUseCase>(),
      ));
  return getIt;
}

class _$InjectableModule extends _i24.InjectableModule {}

class _$ServiceModule extends _i25.ServiceModule {}

class _$DioInjectableModule extends _i26.DioInjectableModule {}

class _$RegisterModule extends _i18.RegisterModule {}
