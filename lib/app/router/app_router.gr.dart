// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouterGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:auto_route/auto_route.dart' as _i4;
import 'package:flutter/material.dart' as _i5;
import 'package:supercharge_ff_kristof/features/detail_beer/presentation/beer_detail_page.dart'
    as _i1;
import 'package:supercharge_ff_kristof/features/list_beers/presentation/page/list_beer_page.dart'
    as _i2;
import 'package:supercharge_ff_kristof/features/random/domain/entities/random_beer_entity.dart'
    as _i6;
import 'package:supercharge_ff_kristof/features/random/presentation/page/random_beer_page.dart'
    as _i3;

abstract class $AppRouter extends _i4.RootStackRouter {
  $AppRouter({super.navigatorKey});

  @override
  final Map<String, _i4.PageFactory> pagesMap = {
    BeerDetailRoute.name: (routeData) {
      final args = routeData.argsAs<BeerDetailRouteArgs>();
      return _i4.AutoRoutePage<void>(
        routeData: routeData,
        child: _i1.BeerDetailPage(
          key: args.key,
          beer: args.beer,
        ),
      );
    },
    ListBeerRoute.name: (routeData) {
      return _i4.AutoRoutePage<void>(
        routeData: routeData,
        child: const _i2.ListBeerPage(),
      );
    },
    RandomBeerRoute.name: (routeData) {
      return _i4.AutoRoutePage<void>(
        routeData: routeData,
        child: const _i3.RandomBeerPage(),
      );
    },
    RandomBeerView.name: (routeData) {
      return _i4.AutoRoutePage<void>(
        routeData: routeData,
        child: const _i3.RandomBeerView(),
      );
    },
  };
}

/// generated route for
/// [_i1.BeerDetailPage]
class BeerDetailRoute extends _i4.PageRouteInfo<BeerDetailRouteArgs> {
  BeerDetailRoute({
    _i5.Key? key,
    required _i6.RandomBeerEntity beer,
    List<_i4.PageRouteInfo>? children,
  }) : super(
          BeerDetailRoute.name,
          args: BeerDetailRouteArgs(
            key: key,
            beer: beer,
          ),
          initialChildren: children,
        );

  static const String name = 'BeerDetailRoute';

  static const _i4.PageInfo<BeerDetailRouteArgs> page =
      _i4.PageInfo<BeerDetailRouteArgs>(name);
}

class BeerDetailRouteArgs {
  const BeerDetailRouteArgs({
    this.key,
    required this.beer,
  });

  final _i5.Key? key;

  final _i6.RandomBeerEntity beer;

  @override
  String toString() {
    return 'BeerDetailRouteArgs{key: $key, beer: $beer}';
  }
}

/// generated route for
/// [_i2.ListBeerPage]
class ListBeerRoute extends _i4.PageRouteInfo<void> {
  const ListBeerRoute({List<_i4.PageRouteInfo>? children})
      : super(
          ListBeerRoute.name,
          initialChildren: children,
        );

  static const String name = 'ListBeerRoute';

  static const _i4.PageInfo<void> page = _i4.PageInfo<void>(name);
}

/// generated route for
/// [_i3.RandomBeerPage]
class RandomBeerRoute extends _i4.PageRouteInfo<void> {
  const RandomBeerRoute({List<_i4.PageRouteInfo>? children})
      : super(
          RandomBeerRoute.name,
          initialChildren: children,
        );

  static const String name = 'RandomBeerRoute';

  static const _i4.PageInfo<void> page = _i4.PageInfo<void>(name);
}

/// generated route for
/// [_i3.RandomBeerView]
class RandomBeerView extends _i4.PageRouteInfo<void> {
  const RandomBeerView({List<_i4.PageRouteInfo>? children})
      : super(
          RandomBeerView.name,
          initialChildren: children,
        );

  static const String name = 'RandomBeerView';

  static const _i4.PageInfo<void> page = _i4.PageInfo<void>(name);
}
