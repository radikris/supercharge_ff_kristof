class AppConstants {
  static int PRELOAD_PAGE = 2;
  static int ANIMATION_DURATION = 500; //ms
  static int PAGE_SIZE = 4; //how many card should we fetch
  static int BEERS_MAX = 10; //how many card should we fetch
}
