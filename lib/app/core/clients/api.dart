import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:retrofit/retrofit.dart';
import 'package:supercharge_ff_kristof/app/core/clients/network/config.dart';
import 'package:supercharge_ff_kristof/app/core/common_models/beer_model.dart';

part 'api.g.dart';

@RestApi()
abstract class ApiClient {
  factory ApiClient(Dio dio, {String? baseUrl}) = _ApiClient;

  @GET('/beers/random')
  Future<List<BeerModel>> getRandomBeer();

  @GET('/beers/{id}')
  Future<List<BeerModel>> getSingleBeer(@Path('id') int id);
}

@module
abstract class RegisterModule {
  ApiClient getService(Dio dio, Config config) {
    return ApiClient(dio, baseUrl: config.baseUrl);
  }
}
