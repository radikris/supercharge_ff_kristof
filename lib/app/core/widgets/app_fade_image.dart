import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:skeletonizer/skeletonizer.dart';
import 'package:supercharge_ff_kristof/gen/assets.gen.dart';
import 'package:transparent_image/transparent_image.dart';

class AppFadeImage extends StatelessWidget {
  const AppFadeImage({
    super.key,
    this.width = double.infinity,
    this.height = double.infinity,
    required this.image,
  });

  final double? width;
  final double? height;
  final String image;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Stack(
      alignment: Alignment.center,
      children: [
        Skeletonizer(
          child: Container(
            height: height,
            width: double.infinity,
            color: theme.splashColor,
          ),
        ),
        FadeInImage.memoryNetwork(
          image: image,
          fit: BoxFit.cover,
          height: height,
          width: width,
          placeholder: kTransparentImage,
          imageErrorBuilder:
              (BuildContext context, Object error, StackTrace? stackTrace) =>
                  Lottie.asset(
            Assets.illustration.notfound,
            height: height,
            width: double.infinity,
            fit: BoxFit.scaleDown,
          ),
        ),
      ],
    );
  }
}
