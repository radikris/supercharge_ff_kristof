import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:supercharge_ff_kristof/app/core/state/app_state.dart';

@singleton
class AppCubitState extends Cubit<AppState> {
  AppCubitState() : super(const AppState.initial());

  void setLoading() {
    emit(const AppState.loading());
  }

  void setError(String error) {
    emit(AppState.error(error));
  }

  void setData() {
    emit(const AppState.data());
  }
}
