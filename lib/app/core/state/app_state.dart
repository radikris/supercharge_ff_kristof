import 'package:freezed_annotation/freezed_annotation.dart';

part 'app_state.freezed.dart';

@freezed
abstract class AppState with _$AppState {
  const factory AppState.initial() = InitialState;
  const factory AppState.loading() = LoadingState;
  const factory AppState.error(String error) = ErrorState;
  const factory AppState.data() = DataState;
}

extension AppStateExtension on AppState {
  bool get isInitial => this is InitialState;
  bool get isLoading => this is LoadingState;
  bool get isError => this is ErrorState;
  bool get isData => this is DataState;

  InitialState get asInitial => this as InitialState;
  LoadingState get asLoading => this as LoadingState;
  ErrorState get asError => this as ErrorState;
  DataState get asData => this as DataState;
}
