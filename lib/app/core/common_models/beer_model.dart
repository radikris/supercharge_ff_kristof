import 'dart:math';

import 'package:json_annotation/json_annotation.dart';

part 'beer_model.g.dart';

@JsonSerializable()
class BeerModel {
  BeerModel({
    required this.id,
    required this.name,
    required this.tagline,
    required this.firstBrewed,
    required this.description,
    required this.imageUrl,
    required this.abv,
    required this.ibu,
    required this.targetFg,
    required this.targetOg,
    required this.ebc,
    required this.srm,
    required this.ph,
    required this.attenuationLevel,
    required this.volume,
    required this.boilVolume,
    required this.method,
    required this.ingredients,
    required this.foodPairing,
    required this.brewersTips,
    required this.contributedBy,
  });

  factory BeerModel.fromJson(Map<String, dynamic> json) =>
      _$BeerModelFromJson(json);

  // JSON keys
  @JsonKey(name: 'id')
  final int? id;
  @JsonKey(name: 'name')
  final String? name;
  @JsonKey(name: 'tagline')
  final String? tagline;
  @JsonKey(name: 'first_brewed')
  final String? firstBrewed;
  @JsonKey(name: 'description')
  final String? description;
  @JsonKey(name: 'image_url')
  final String? imageUrl;
  @JsonKey(name: 'abv')
  final double? abv;
  @JsonKey(name: 'ibu')
  final double? ibu;
  @JsonKey(name: 'target_fg')
  final double? targetFg;
  @JsonKey(name: 'target_og')
  final double? targetOg;
  @JsonKey(name: 'ebc')
  final double? ebc;
  @JsonKey(name: 'srm')
  final double? srm;
  @JsonKey(name: 'ph')
  final double? ph;
  @JsonKey(name: 'attenuation_level')
  final double? attenuationLevel;
  @JsonKey(name: 'volume')
  final Volume? volume;
  @JsonKey(name: 'boil_volume')
  final Volume? boilVolume;
  @JsonKey(name: 'method')
  final Method? method;
  @JsonKey(name: 'ingredients')
  final Ingredients? ingredients;
  @JsonKey(name: 'food_pairing')
  final List<String>? foodPairing;
  @JsonKey(name: 'brewers_tips')
  final String? brewersTips;
  @JsonKey(name: 'contributed_by')
  final String? contributedBy;
  Map<String, dynamic> toJson() => _$BeerModelToJson(this);

  static BeerModel get mock => BeerModel(
        id: Random().nextInt(100),
        name: 'Test Beer',
        tagline: 'Tagline ',
        description: 'Description',
        imageUrl: '',
        abv: 5,
        ibu: 20,
        targetFg: 1010,
        targetOg: 1055,
        ebc: 15,
        srm: 8,
        ph: 4.2,
        attenuationLevel: 75,
        volume: Volume(value: 20, unit: 'liters'),
        boilVolume: Volume(value: 25, unit: 'liters'),
        method: Method(
          twist: 'Twist,',
          mashTemp: [
            MashTemp(temp: Temp(value: 65, unit: 'celsius'), duration: 60),
          ],
          fermentation: Fermentation(temp: Temp(value: 18, unit: 'celsius')),
        ),
        ingredients: Ingredients(
          malt: [Malt(name: 'Malt', amount: Amount(value: 2, unit: 'kg'))],
          hops: [
            Hop(
              name: 'Hop',
              amount: Amount(value: 10, unit: 'grams'),
              add: 'start',
              attribute: 'bitter',
            ),
          ],
          yeast: 'Yeast ',
        ),
        foodPairing: ['Food Pairing'],
        brewersTips: 'Brewers Tips',
        contributedBy: 'Contributor',
        firstBrewed: '1950',
      );
}

@JsonSerializable()
class Volume {
  Volume({
    required this.value,
    required this.unit,
  });

  factory Volume.fromJson(Map<String, dynamic> json) => _$VolumeFromJson(json);

  @JsonKey(name: 'value')
  final double? value;
  @JsonKey(name: 'unit')
  final String? unit;
  Map<String, dynamic> toJson() => _$VolumeToJson(this);
}

@JsonSerializable()
class Method {
  Method({
    required this.mashTemp,
    required this.fermentation,
    required this.twist,
  });

  factory Method.fromJson(Map<String, dynamic> json) => _$MethodFromJson(json);

  @JsonKey(name: 'mash_temp')
  final List<MashTemp>? mashTemp;
  @JsonKey(name: 'fermentation')
  final Fermentation? fermentation;
  @JsonKey(name: 'twist')
  final String? twist;
  Map<String, dynamic> toJson() => _$MethodToJson(this);
}

@JsonSerializable()
class MashTemp {
  MashTemp({
    required this.temp,
    required this.duration,
  });

  factory MashTemp.fromJson(Map<String, dynamic> json) =>
      _$MashTempFromJson(json);

  @JsonKey(name: 'temp')
  final Temp? temp;
  @JsonKey(name: 'duration')
  final int? duration;
  Map<String, dynamic> toJson() => _$MashTempToJson(this);
}

@JsonSerializable()
class Temp {
  Temp({
    required this.value,
    required this.unit,
  });

  factory Temp.fromJson(Map<String, dynamic> json) => _$TempFromJson(json);

  @JsonKey(name: 'value')
  final double? value;
  @JsonKey(name: 'unit')
  final String? unit;
  Map<String, dynamic> toJson() => _$TempToJson(this);
}

@JsonSerializable()
class Fermentation {
  Fermentation({
    required this.temp,
  });

  factory Fermentation.fromJson(Map<String, dynamic> json) =>
      _$FermentationFromJson(json);

  @JsonKey(name: 'temp')
  final Temp? temp;
  Map<String, dynamic> toJson() => _$FermentationToJson(this);
}

@JsonSerializable()
class Ingredients {
  Ingredients({
    required this.malt,
    required this.hops,
    required this.yeast,
  });

  factory Ingredients.fromJson(Map<String, dynamic> json) =>
      _$IngredientsFromJson(json);

  @JsonKey(name: 'malt')
  final List<Malt>? malt;
  @JsonKey(name: 'hops')
  final List<Hop>? hops;
  @JsonKey(name: 'yeast')
  final String? yeast;
  Map<String, dynamic> toJson() => _$IngredientsToJson(this);
}

@JsonSerializable()
class Malt {
  Malt({
    required this.name,
    required this.amount,
  });

  factory Malt.fromJson(Map<String, dynamic> json) => _$MaltFromJson(json);

  @JsonKey(name: 'name')
  final String? name;
  @JsonKey(name: 'amount')
  final Amount? amount;
  Map<String, dynamic> toJson() => _$MaltToJson(this);
}

@JsonSerializable()
class Hop {
  Hop({
    required this.name,
    required this.amount,
    required this.add,
    required this.attribute,
  });

  factory Hop.fromJson(Map<String, dynamic> json) => _$HopFromJson(json);

  @JsonKey(name: 'name')
  final String? name;
  @JsonKey(name: 'amount')
  final Amount? amount;
  @JsonKey(name: 'add')
  final String? add;
  @JsonKey(name: 'attribute')
  final String? attribute;
  Map<String, dynamic> toJson() => _$HopToJson(this);
}

@JsonSerializable()
class Amount {
  Amount({
    required this.value,
    required this.unit,
  });

  factory Amount.fromJson(Map<String, dynamic> json) => _$AmountFromJson(json);

  @JsonKey(name: 'value')
  final double? value;
  @JsonKey(name: 'unit')
  final String? unit;
  Map<String, dynamic> toJson() => _$AmountToJson(this);
}
