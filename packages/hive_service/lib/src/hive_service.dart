import 'dart:developer';

import 'package:hive_flutter/hive_flutter.dart';
import 'package:hive_service/hive_service.dart';
import 'package:supercharge_ff_kristof/features/random/domain/entities/random_beer_entity.dart';

class HiveService implements IHiveService {
  HiveService({
    required String name,
    HiveInterface? hive,
  })  : _hive = hive ?? Hive,
        _name = name;

  final HiveInterface _hive;
  final String _name;

  @override
  Future<Box<dynamic>> get box async {
    print('box init kris $_name');
    try {
      return _hive.box<dynamic>(_name);
    } catch (e) {
      await initializeHive();
      print('box init done $_name');

      final res = _hive.box<dynamic>(_name);
      print(res);
      return res;
    }
  }

  @override
  Future<void> initializeHive() async {
    try {
      print('init hive $_name');
      await _hive.initFlutter();
      Hive.registerAdapter(RandomBeerEntityAdapter());
      print('box init init flutter');

      print('register adapter');

      await _hive.openBox<dynamic>(_name);
      print('opened box $_name');
    } catch (e, s) {
      log('$e unable to initialize hive', error: e, stackTrace: s);
    }
  }
}
