import 'package:hive/hive.dart';
import 'package:mocktail/mocktail.dart';
import 'package:supercharge_ff_kristof/features/list_beers/domain/usecases/clear_saved_beer_usecase.dart';
import 'package:supercharge_ff_kristof/features/list_beers/domain/usecases/read_saved_beer_usecase.dart';
import 'package:supercharge_ff_kristof/features/random/domain/entities/random_beer_entity.dart';
import 'package:supercharge_ff_kristof/features/random/domain/repositories/local_storage_repository.dart';
import 'package:supercharge_ff_kristof/features/random/domain/usecases/save_beer_usecase.dart';
import 'package:test/test.dart';

class MockLocalStorageRepository extends Mock
    implements ILocalStorageRepository {}

void main() {
  setUpAll(() {
    registerFallbackValue(MockBox());
  });

  group('ReadSavedBeerUseCase', () {
    test(
        'should return a list of beers when successfully read from local storage',
        () async {
      final repository = MockLocalStorageRepository();
      final useCase = ReadSavedBeerUseCase(repository);
      final expectedBeers = [
        RandomBeerEntity(
          id: 1,
          name: 'Beer 1',
          description: '',
          image: '',
          tagline: '',
        ),
        RandomBeerEntity(
          id: 2,
          name: 'Beer 2',
          description: '',
          image: '',
          tagline: '',
        ),
      ];

      when(repository.openBox).thenAnswer((_) async => MockBox());
      when(() => repository.getBeers(any())).thenReturn(expectedBeers);

      final result = await useCase.call();

      expect(result, equals(expectedBeers));
      verify(repository.openBox).called(1);
      verify(() => repository.getBeers(any())).called(1);
    });

    test('should throw an error when failed to read from local storage',
        () async {
      final repository = MockLocalStorageRepository();
      final useCase = ReadSavedBeerUseCase(repository);

      when(repository.openBox).thenThrow(Exception('Failed to open box'));

      expect(useCase.call, throwsException);
      verify(repository.openBox).called(1);
    });
  });

  group('SaveBeerUseCase', () {
    test('should successfully save a beer to local storage', () async {
      final repository = MockLocalStorageRepository();
      final useCase = SaveBeerUseCase(repository);
      final beerToSave = RandomBeerEntity(
        id: 1,
        name: 'Beer 1',
        description: '',
        image: '',
        tagline: '',
      );

      when(repository.openBox).thenAnswer((_) async => MockBox());

      await useCase.call(beerToSave);

      verify(repository.openBox).called(1);
      verify(() => repository.addBeer(any(), beerToSave)).called(1);
    });

    test('should throw an error when failed to save a beer to local storage',
        () async {
      final repository = MockLocalStorageRepository();
      final useCase = SaveBeerUseCase(repository);
      final beerToSave = RandomBeerEntity(
        id: 1,
        name: 'Beer 1',
        description: '',
        image: '',
        tagline: '',
      );

      when(repository.openBox).thenThrow(Exception('Failed to open box'));

      expect(() => useCase.call(beerToSave), throwsException);
      verify(repository.openBox).called(1);
    });
  });

  group('ClearSavedBeerUseCase', () {
    test('should successfully clear saved beers from local storage', () async {
      final repository = MockLocalStorageRepository();
      final useCase = ClearSavedBeerUseCase(repository);

      when(repository.openBox).thenAnswer((_) async => MockBox());

      await useCase.call();

      verify(repository.openBox).called(1);
      verify(() => repository.clearBeers(any())).called(1);
    });

    test(
        'should throw an error when failed to clear saved beers from local storage',
        () async {
      final repository = MockLocalStorageRepository();
      final useCase = ClearSavedBeerUseCase(repository);

      when(repository.openBox).thenThrow(Exception('Failed to open box'));

      expect(useCase.call, throwsException);
      verify(repository.openBox).called(1);
    });
  });
}

class MockBox extends Mock implements Box<RandomBeerEntity> {}
